from glob import glob
from os import path
from shutil import move


def _prepare_subsection(data: dict, sections: list):
    current = data
    for i in sections[:-1]:
        current = current[i]

    current[sections[-1]] = dict()
    return current[sections[-1]]


def load_manifest(manifest_text: str):
    parsed = dict()
    current_section = parsed
    sections = []

    for line in (i.strip() for i in manifest_text.split("\n")):
        try:
            key, value = line.split(None, 1)
            key = key.replace('"', '').lstrip()
            value = value.replace('"', '').rstrip()
        except ValueError:
            if line == "{":
                current_section = _prepare_subsection(parsed, sections)
            elif line == "}":
                sections.pop()
            else:
                sections.append(line.replace('"', ''))
            continue
        current_section[key] = value

    return parsed["AppState"]


class SteamGame:
    def __init__(self, steam_folder: str, filename: str):
        self.filename = filename
        self.steam_folder = steam_folder
        self.manifest_file = path.join(self.steam_folder, "steamapps", self.filename)
        self.manifest = load_manifest(open(self.manifest_file).read())
        self.appid = self.manifest["appid"]
        self.folder_name = self.manifest["installdir"]
        self.installed_folder = path.join(steam_folder, "steamapps", "common", self.folder_name)
        self.name = self.manifest["name"]

    def move_to(self, new_folder: str):
        self.steam_folder = new_folder
        move(self.manifest_file, path.join(new_folder, "steamapps"))
        self.manifest_file = path.join(self.steam_folder, "steamapps", self.filename)
        move(self.installed_folder, path.join(new_folder, "steamapps", "common"))
        self.installed_folder = path.join(new_folder, "steamapps", "common", self.folder_name)


def get_games(steam_folders: list):
    found_games = list()

    for folder in steam_folders:
        for f in glob(path.join(folder, "steamapps", "*.acf")):
            found_games.append(SteamGame(folder, f))

    return found_games

def find_game(q: str, games: list, i=0):
    return [i for i in games if q.lower() in i.name.lower()][i]
