#!/usr/bin/env python3

from sys import exit, stderr
from Utils.Common import progress_bar, RS

PLATFORM_MAP = {
    "linux": "Linux",
    "windows": "Windows",
    "mac": "MacOS",
    "android": "Android",
    "audio": "Sound",
    "ebook": "Book",
    "asmjs": "ASM.JS"
}


def scrape_games():
    try:
        orders = RS.get("https://humblebundle.com/api/v1/user/order").json()
        orders = [i["gamekey"] for i in orders]
    except:
        stderr.write("Invalid cookies, please check your config file\n")
        exit(1)

    games = list()

    progress_bar(0, len(orders))

    for x, order in enumerate(orders):
        r = RS.get(f"https://humblebundle.com/api/v1/order/{order}").json()

        for s in r["subproducts"]:
            if len(s["downloads"]) > 0:
                games.append(s)

        progress_bar(x + 1, len(orders))

    stderr.write("\n")

    to_dl = list()

    progress_bar(0, len(games))

    for x, game in enumerate(games):
        name = game["human_name"]
        for download in game["downloads"]:
            os = PLATFORM_MAP[download["platform"]]
            for file in download["download_struct"]:
                if "url" not in file:
                    continue
                url = file["url"]["web"]
                filename = url.split("/")[-1].split("?")[0]
                filepath = f"{os}/{name}/{filename}"
                c_d = {"in": url, "out": filepath}
                if "md5" in file:
                    c_d["md5"] = file["md5"]
                if "sha1" in file:
                    c_d["sha1"] = file["sha1"]
                to_dl.append(c_d)
        progress_bar(x, len(games))

    return to_dl
