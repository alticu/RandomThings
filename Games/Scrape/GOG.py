from urllib.parse import unquote
from sys import exit
from Utils.Common import progress_bar, purify, RS

TYPE_MAP = {
    "windows": "Windows",
    "mac": "MacOS",
    "linux": "Linux",
    "audio": "Sound",
    "manuals": "Manual",
    "wallpapers": "Wallpaper",
    "avatars": "Avatar",
    "game add-ons": "Addons",
    "guides & reference ": "Guide",
    "video": "Video",
    "artworks": "Artwork"
}


def follow_redir(url: str):
    r = RS.get(url, stream=True)
    r.close()
    return r.url


def scrape_games():
    try:
        games = RS.get("https://embed.gog.com/user/data/games").json()["owned"]
    except:
        print("Invalid cookies!")
        exit(1)

    to_dl = list()

    progress_bar(0, len(games))

    for x, game_id in enumerate(games):
        game = RS.get(f"https://embed.gog.com/account/gameDetails/{game_id}.json").json()
        name = purify(game["title"])
        downloads = game["downloads"][0][1]
        for os in downloads.keys():
            os_name = TYPE_MAP[os]

            for download in downloads[os]:
                if download["version"] and "->" in download["version"]:
                    # Ignore patches
                    continue
                download_url = follow_redir("https://embed.gog.com" + download["manualUrl"])
                filename = purify(unquote(download_url.split("/")[-1].split("?")[0]))

                to_dl.append({"in": download_url, "out": f"{os_name}/{name}/{filename}"})

        for extra in game["extras"]:
            try:
                extra_type = TYPE_MAP[extra["type"]]
            except:
                extra_type = input(f"Foldername for \"{extra['type']}\": ")
                TYPE_MAP[extra["type"]] = extra_type

            download_url = follow_redir("https://embed.gog.com" + extra["manualUrl"])
            filename = purify(unquote(download_url.split("/")[-1].split("?")[0]))

            to_dl.append({"in": download_url, "out": f"{extra_type}/{name}/{filename}"})
        progress_bar(x + 1, len(games))

    return to_dl
