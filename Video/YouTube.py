#!/usr/bin/env python3
"""Parse YouTube subscriptions file"""

from re import findall
from typing import List, Dict, Tuple, Any

from feedparser import parse

class YouTubeThumbnail:
    URL: str
    Width: int
    Height: int

    def __repr__(self):
        return "<YouTubeThumbnail %ix%i>" % (self.Width, self.Height)

class YouTubeRatings:
    Count: int
    Average: float
    Views: int

    def __repr__(self):
        return "<YouTubeRatings %f>" % (self.Average)

class YouTubeVideo:
    ID: str
    Link: str
    Channel: str
    Title: str
    Published: str
    Thumbnail: YouTubeThumbnail
    Summary: str
    Ratings: YouTubeRatings

    def __init__(self, obj: dict):
        self.ID = obj["yt_videoid"]
        self.Link = obj["link"]
        self.Channel = obj["yt_channelid"]
        self.Title = obj["title"]
        self.Published = obj["published"]
        self.Summary = obj["summary"]

        self.Thumbnail = YouTubeThumbnail()

        thumbObj = obj["media_thumbnail"][0]
        self.Thumbnail.URL = thumbObj["url"]
        self.Thumbnail.Width = thumbObj["width"]
        self.Thumbnail.Height = thumbObj["height"]

        self.Ratings = YouTubeRatings()
        
        self.Ratings.Count = obj["media_starrating"]["count"]
        self.Ratings.Average = obj["media_starrating"]["average"]
        self.Ratings.Views = obj["media_statistics"]["views"]

    def __lt__(self, obj):
        return self.Published < obj.Published

    def __gt__(self, obj):
        return self.Published > obj.Published

    def __eq__(self, obj):
        return self.ID == obj.ID

    def __le__(self, obj):
        return self.Published <= obj.Published

    def __ge__(self, obj):
        return self.Published >= obj.Published

    def __repr__(self):
        return "<YouTubeVideo \"%s\">" % self.Title


def read_from_file(filename: str) -> Tuple[List[str], List[str]]:
    with open(filename) as f:
        SUB_TEXT: str = f.read()

    TITLES: List[str] = findall("title=\"(.+?)\"", SUB_TEXT)[1:] # 1st match is the title of the file
    FEEDS: List[str] = findall("xmlUrl=\"(.+?)\"", SUB_TEXT)

    assert len(TITLES) == len(FEEDS), "Title and feed count don't match"
    
    return (TITLES, FEEDS)

def retrive_contents(feeds: Tuple[List[str], List[str]]) -> List[YouTubeVideo]:
    """Retrive contents of all subscription feeds in chronological order"""
    VIDEOS: List[YouTubeVideo] = []

    for x, i in enumerate(feeds[0]):
        print("Downloading feed for %s" % i)
        feed = parse(feeds[1][x])
        print("Parsing %s" % i)
        for vid in feed["entries"]:
            VIDEOS.append(YouTubeVideo(vid))

    return VIDEOS

def print_videos(videos: List[YouTubeVideo], offset: int = 0, count: int = 25):
    videos: List[YouTubeVideo] = videos[offset * count:offset * count + count]
    for x, vid in enumerate(videos):
        print("%i: %s" % (offset * count + x, vid.Title))
