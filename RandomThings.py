#!/usr/bin/env python3
# Common launcher for all RandomThings

from sys import argv, exit
from os import path, cpu_count
from json import dumps, loads
from platform import system
from argparse import ArgumentParser
from Music.Scrape import Ninety9Lives, Bandcamp
from Music.Compress import CompressAll
from Games.Scrape import GOG, HumbleBundle
from Utils.DownloadFiles import download_files
from Utils.Common import RS


def download_99l():
    parser = ArgumentParser(description="Download music from Ninety9Lives")
    parser.add_argument("-q", "--query",
                        help="Filter songs by category")
    parser.add_argument("-o", "--out-folder",
                        help="Folder to download to")
    args = parser.parse_args()

    print("Scraping Download URLs")
    urls = Ninety9Lives.get_by_query(args.query)
    print("Downloading Files")
    return download_files(urls, args.out_folder)


def download_bandcamp():
    parser = ArgumentParser(description="Download music from Bandcamp")
    parser.add_argument("url", help="Url of Bandcamp page to scrape")
    parser.add_argument("-o", "--out-folder",
                        help="Folder to download to")
    args = parser.parse_args()

    print("Scraping Download URLs")
    urls = Bandcamp.scrape_from_page(args.url)
    print("Downloading Files")
    return download_files(urls, args.out_folder)


def compress():
    parser = ArgumentParser(description="Compress all music files using Opus VBR")
    parser.add_argument("input", help="Input folder")
    parser.add_argument("-o", "--out-folder",
                        help="Folder to compress to")
    parser.add_argument("-s", "--scaled-size", default="256x256", help="Size to rescale cover to (blank to disable)")
    parser.add_argument("-q", "--quality", default=10, type=int, help="Output quality: (0-10, 10: highest)")
    parser.add_argument("-b", "--bitrate", default=128, type=int, help="Output bitrate (average in VBR mode)")
    parser.add_argument("-t", "--threads", default=cpu_count(), type=int, help="Worker threads for encoding")
    args = parser.parse_args()

    return CompressAll.compress_all(args.input, args.out_folder, args.threads, args.quality, args.bitrate)


def download_hb():
    RS.cookies.update(CONFIG["cookies"]["hb"])
    parser = ArgumentParser(description="Download games from HumbleBundle")
    parser.add_argument("-o", "--out-folder",
                        help="Folder to download to")
    args = parser.parse_args()

    print("Scraping Download URLs")
    urls = HumbleBundle.scrape_games()
    print("Downloading Files")
    return download_files(urls, args.out_folder)


def download_gog():
    RS.cookies.update(CONFIG["cookies"]["gog"])
    parser = ArgumentParser(description="Download games from GOG")
    parser.add_argument("-o", "--out-folder",
                        help="Folder to download to")
    args = parser.parse_args()

    print("Scraping Download URLs")
    urls = GOG.scrape_games()
    print("Downloading Files")
    return download_files(urls, args.out_folder)


EXEC_MAP = {
    "download_99l": download_99l,
    "download_bandcamp": download_bandcamp,
    "compress": compress,
    "download_hb": download_hb,
    "download_gog": download_gog
}

if system() == "Linux":
    from xdg import XDG_CONFIG_HOME
    CONFIG_FOLDER = XDG_CONFIG_HOME
else:
    CONFIG_FOLDER = path.expanduser("~")
CONFIG_FILE = path.join(CONFIG_FOLDER, "RandomThings.json")

if not path.exists(CONFIG_FILE):
    open(CONFIG_FILE, "w+").write(dumps({
        "cookies": {
            "gog": {
                "gog-al": ""
            },
            "hb": {
                "_simpleauth_sess": "",
                "hbguard": ""
            }
        }
    }, indent=4))

CONFIG = loads(open(CONFIG_FILE).read())

if path.basename(argv[0]) in EXEC_MAP:
    exit(EXEC_MAP[path.basename(argv[0])]())
elif len(argv) > 1 and argv[1] in EXEC_MAP:
    exec_name = argv[1]
    argv[0] = argv[0] + " " + argv[1]
    del argv[1]
    exit(EXEC_MAP[exec_name]())
else:
    print(f"Usage: {argv[0]} <module to run>")
    print("Available Modules:")
    for module in EXEC_MAP.keys():
        print(f"\t{module}")
    exit(1)
