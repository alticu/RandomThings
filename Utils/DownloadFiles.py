from sys import stderr
from os import path, remove
from distutils.dir_util import mkpath
from Utils.Common import download, progress_bar


def download_files(dl_data: list, out_path: str):
    to_dl = list()

    for file in dl_data:
        if out_path:
            file["out"] = path.join(out_path, file["out"])
        if not path.exists(file["out"]):
            to_dl.append(file)

    for x, FILE in enumerate(to_dl):
        out_dir = path.dirname(FILE["out"])
        if not path.exists(out_dir):
            mkpath(out_dir)
        try:
            download(FILE["in"], FILE["out"])
        except:
            remove(FILE["out"])
            stderr.write(f"Failed to download {FILE['out']}!\n")
            return 1
        progress_bar(x + 1, len(to_dl))

    return 0
