from sys import stderr, stdout
from urllib.request import urlopen, Request
from shutil import get_terminal_size, copyfileobj
from requests import Session

USER_AGENTS = {
        "FF57-Win10": "Mozilla/5.0 (Windows NT 10.0; Win64; rv:57.2) Gecko/20100101 Firefox/57.2"
}

SAFE_CHARS = [" ", ".", "_", "-", "'", ",", ":"]

RS = Session()
RS.headers.update({"User-Agent": USER_AGENTS["FF57-Win10"]})


class Colors:
    IS_TTY = stdout.isatty() and stderr.isatty()

    R = "\033[1;31m" if IS_TTY else ""
    Y = "\033[1;33m" if IS_TTY else ""
    G = "\033[1;32m" if IS_TTY else ""
    C = "\033[0m" if IS_TTY else ""


def progress_bar(c: int, m: int):
    bars = get_terminal_size().columns - (4 + len(f"{c}/{m}"))
    current = round((bars / m) * c)
    stderr.write(f"\r"
                 f"[{Colors.G}{'=' * current}>{Colors.C}"
                 f"{Colors.R}{'-' * (bars - current)}{Colors.C}] "
                 f"{Colors.R}{c}{Colors.C}/{Colors.G}{m}{Colors.C}")


def purify(value: str):
    return "".join([i for i in value if i.isalnum() or i in SAFE_CHARS]).rstrip()


def download(url: str, filename: str):
    req = Request(url=url, headers={"User-Agent": USER_AGENTS["FF57-Win10"]})
    with urlopen(req) as res, open(filename, "wb+") as f:
        copyfileobj(res, f)
