from subprocess import call, DEVNULL
from multiprocessing.pool import Pool
from multiprocessing import Manager, Queue
from os import path, remove, close
from glob import glob
from distutils.dir_util import mkpath
from sys import stderr
from tempfile import mkstemp
from Utils.Common import progress_bar


VALID_EXTENSIONS = [
    "ogg",
    "oga",
    "flac",
    "mp3",
    "wav",
    "opus"
]

OUT_DIR = ""
IN_DIR = ""
QUALITY = 10
BITRATE = 128
SCALED_SIZE = "256x256"

def call_check(command: list, msg: str):
    cmstr = ' '.join(command)
    # print("CALL", cmstr, msg) # uncomment to view commands
    CODE = call(command, stdout=DEVNULL, stderr=DEVNULL)
    if CODE != 0:
        print(f'FAIL {cmstr}')
        raise Exception(msg + " failed!")

def convert_file(source: str, dest: str):
    try:
        # call_check(["opusenc", "--vbr", "--comp", str(QUALITY), "--bitrate", str(BITRATE), source, dest], 'encoding file')
        call_check(["ffmpeg", "-i", source, "-vn", "-acodec", "libopus", "-b:a", f"{BITRATE}k", "-vbr", "on", "-compression_level", str(QUALITY), '-map_metadata', '0', dest], 'encoding file')
    except Exception as e:
        stderr.write(e.with_traceback())
        stderr.write(f"\nfailed to convert {source} to {dest}!\n")
        remove(dest)


def find_files(parent_dir: str):
    FOUND = []
    for ext in VALID_EXTENSIONS:
        FOUND += glob(path.join(parent_dir, f"**/*.{ext}"), recursive=True)
    return [i[len(parent_dir) + 1 if parent_dir[-1] != "/" else 0:] for i in FOUND]


def process_file(f: str, q: Queue):
    name, _ = path.splitext(f)
    out_file = path.join(OUT_DIR, f"{name}.ogg")
    out_dir = path.join(OUT_DIR, path.dirname(f))

    if path.exists(out_file):
        q.put(1)
        return

    if not path.exists(out_dir):
        mkpath(out_dir)

    convert_file(path.join(IN_DIR, f), out_file)
    q.put(1)


def compress_all(in_dir: str, out_dir: str, threads: int, quality: int, bitrate: int):
    global OUT_DIR, IN_DIR
    QUALITY = quality
    BITRATE = bitrate
    if out_dir:
        OUT_DIR = out_dir
    IN_DIR = in_dir

    found_files = find_files(in_dir)
    if len(found_files) == 0:
        print(f"no usable input files found, exiting.")
        return
    print(f"found {len(found_files)} files to convert")
    print(f"re-encoding files using codec *opus[vbr]*, quality {QUALITY}, bitrate {BITRATE}kbps")
    print(f"creating {threads} worker processes")

    m = Manager()
    q = m.Queue()
    total_done = 0

    progress_bar(total_done, len(found_files))

    with Pool() as p:
        p.starmap_async(process_file, [(i, q) for i in found_files])
        while total_done < len(found_files):
            total_done += q.get()
            progress_bar(total_done, len(found_files))

    return 0
