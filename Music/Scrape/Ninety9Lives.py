from re import search
from json import loads
from urllib.parse import quote
from Utils.Common import progress_bar, RS


def get_by_query(query: str):
    r = RS.get("https://www.ninety9lives.com/music/").text
    parsed = loads(search("<script> window.preloadTracks =(.+)</script>", r).group(1))
    if query:
        parsed = [i for i in parsed if query in i['search']]

    urls = list()
    for x, i in enumerate(parsed):
        page = RS.get(f"https://www.ninety9lives.com/music/{i['slug']}/").text
        filename = search('file: "(.+)", slu', page).group(1)
        link = "http://cdn.ninety9lives.com/" + quote(filename)
        urls.append({"in": link, "out": filename})
        progress_bar(x + 1, len(parsed))

    return urls
