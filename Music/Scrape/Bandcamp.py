#!/usr/bin/env python3
from re import search
from json import loads
from Utils.Common import purify, RS


def scrape_from_page(page: str):
    page = RS.get(page).text
    track_info = loads(search('(?<=trackinfo: )(\[.*\])', page).group(1))

    print(f"Found {len(track_info)} songs")

    to_dl = list()

    for track in track_info:
        to_dl.append({"in": track["file"]["mp3-128"], "out": purify(track["title"]) + ".mp3"})

    return to_dl
